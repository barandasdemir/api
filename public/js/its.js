/* eslint-disable no-undef */
window.addEventListener('load', () => {
    let selectedDeviceId;
    const codeReader = new ZXing.BrowserMultiFormatReader();
    codeReader.getVideoInputDevices()
        .then((videoInputDevices) => {
            const sourceSelect = document.getElementById('sourceSelect');
            selectedDeviceId = videoInputDevices[0].deviceId;
            if (videoInputDevices.length >= 1) {
                videoInputDevices.forEach((element) => {
                    const sourceOption = document.createElement('option');
                    sourceOption.text = element.label;
                    sourceOption.value = element.deviceId;
                    sourceSelect.appendChild(sourceOption);
                });

                sourceSelect.onchange = () => {
                    selectedDeviceId = sourceSelect.value;
                };

                const sourceSelectPanel = document.getElementById('sourceSelectPanel');
                sourceSelectPanel.style.display = 'block';
            }

            document.getElementById('startButton').addEventListener('click', () => {
                codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
                    if (result) {
                        const code = result
                            .text
                            .substring(1, result.text.length)
                            .replace(new RegExp(String.fromCharCode(29), 'g'), '-');
                        window.location.replace(`/its/${code}`);
                    }
                    if (err && !(err instanceof ZXing.NotFoundException)) {
                        document.getElementById('error').textContent = err;
                    }
                });
            });

            document.getElementById('resetButton').addEventListener('click', () => {
                codeReader.reset();
            });
        })
        .catch((err) => {
            document.getElementById('err').innerText = err;
        });
});
