const express = require('express');
const axios = require('axios');
const sha1 = require('js-sha1');
const parser = require('fast-xml-parser');

const router = express.Router();

// https://github.com/zxing-js/library/blob/master/docs/examples/multi-camera/index.html
router.get('/', (req, res) => {
    res.render('its/index');
});

router.get('/:barcode', (req, res, next) => {
    const matrix = req.params.barcode.split('-')[0];
    let gtin;
    let sn;

    if (matrix.indexOf('01') === 0) {
        gtin = matrix.substring(2, 16);
        sn = matrix.substring(18, matrix.length);
    }
    if (matrix.indexOf('21') === 0) {
        sn = matrix.substring(2, matrix.length - 16);
        gtin = matrix.substring(matrix.length - 14, 14);
    }

    const hash = sha1(gtin + sn);
    const check = sha1(hash.substring(5, 21) + hash.substring(3, 12));

    const envelope = `<v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/" xmlns:v="http://schemas.xmlsoap.org/soap/envelope/"><v:Header /><v:Body><n0:CheckProductStatusRequest id="o0" c:root="1" xmlns:n0="http://its.titck.gov.tr/net/check/productstatus/"><GTIN i:type="d:string">${gtin}</GTIN><SN i:type="d:string">${sn}</SN><CHECK i:type="d:string">${check}</CHECK><ISMANUEL i:type="d:string">1</ISMANUEL><USERKEY i:type="d:string">eb336d3b7a216fe8</USERKEY><DEVICE i:type="d:string">1</DEVICE></n0:CheckProductStatusRequest></v:Body></v:Envelope>`;

    axios({
        method: 'post',
        url: 'http://its.saglik.gov.tr/ControlProduct/CheckProductStatusService',
        data: envelope,
        headers: {
            'User-Agent': 'kSOAP/2.0',
            Accept: 'text/xml',
            SOAPAction: 'http://its.titck.gov.tr/net/check/productstatus/CheckProductStatusRequest',
            'Content-Type': 'text/xml',
            'Accept-Encoding': 'gzip',
        },
    })
        .then((response) => {
            const {
                data,
            } = response;
            const parsed = parser.parse(data);

            const {
                PRODUCTSTATUS: status,
                PRODUCTIONDATE: prod,
                PRODUCTEXPIREDATE: exp,
                DRUGSUPPLIERNAME: supplier,
                DRUGNAME: name,
                PRICE: price,
            } = parsed['S:Envelope']['S:Body']['ns2:CheckProductStatusResponse'];

            res.send({
                gtin,
                sn,
                drug: {
                    name,
                    supplier,
                    prod,
                    exp,
                    status,
                    price: `${price} ₺`,
                },
            });
        })
        .catch((err) => next(err));
});

module.exports = router;
